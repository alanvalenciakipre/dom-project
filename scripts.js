// click event--------------------------------

const questionContainer =document.querySelector('.click-event');
const btn1 = document.querySelector('#btn-1');
const btn2 =document.getElementById("btn-2");
const response = document.querySelector('p');



questionContainer.addEventListener("click", () => {
  // questionContainer.style.background = "red";
  questionContainer.classList.toggle("question-clicked")
});


btn1.addEventListener("click", () =>{
  //response.style.visibility = "visible";
  response.classList.add('show-response');
  response.style.background = "green";

});

btn2.addEventListener("click", () => {
  //response.style.visibility = "visible";
  response.classList.add('show-response');
  response.style.background = "red";
});


// Mouse Events

const mouseMove = document.querySelector(".mousemove");

 window.addEventListener("mousemove", (event) => {
   mouseMove.style.left = event.pageX + "px";
   mouseMove.style.top = event.pageY + "px";
 });

 window.addEventListener("mousedown", () => {
  mouseMove.style.transform = "sclale(2) translate(-25%, -25%)";
 })

window.addEventListener("mouseup", () => {
  mouseMove.style.transform = "sclale(1) translate(-50%, -50%)";
  mouseMove.style.border = "2px solid teal";
})

questionContainer.addEventListener('mouseenter', () => {
  questionContainer.style.background = "rgba(0,0,0,0.6)";
})

questionContainer.addEventListener('mouseout', () => {
  questionContainer.style.background = "pink";
});

/// KEYPRESS


 const kepressContainer = document.querySelector(".keypress");
const key = document.getElementById("key");

document.addEventListener('keypress', (event) => {
 // console.log(event.key);
 key.textContent = event.key;

    if (event.key === "j") {
      kepressContainer.style.background = "green";
    } else if ((event.key === "h")) {
      kepressContainer.style.background = "teal";
    } else {
      kepressContainer.style.background = "red";
    }
});


// scroll event

const nav = document.querySelector("nav");
window.addEventListener('scroll', () => {
  console.log(window.scrollY);

  if (window.scrollY > 120) {
    nav.style.top = 0;
  }else {
    nav.style.top = "-50px";
  }
});


// les évènements sur les formulaires (Form)

const inputName = document.querySelector('input[type="text"]');
const select = document.querySelector("select");
const form = document.querySelector("form");
let pseudo = "";
let language = "";

inputName.addEventListener("input", (event) => {
 pseudo= console.log(event.target.value);
});

select.addEventListener("input", (event) => {
  language = event.target.value;
})

form.addEventListener("submit", (event) => {
  event.preventDefault(); // empêche le changement de page par defaut de la page

  if (cgv.checked) {
    document.querySelector('form > div').innerHTML = `
     <h3>Pseudo : ${pseudo}</h3
    <h4>Langage préféré: ${language}</h4>
    `;
  }else{
    alert("veillez accepter les CGV")
  }
})

//------------------------------------------------------

// For each ( pour chacun)
const boxes = document.querySelectorAll(".box");
boxes.forEach((box) => {
  box.addEventListener('click', (event) => {
    console.log(event.target);
  });

});
